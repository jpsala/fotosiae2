import environment from './environment';
import {PLATFORM} from 'aurelia-pal';
import 'babel-polyfill';
import {AuthService} from './services/auth';
import {ConfigureHttp} from './resources/helpers/configure-http';
// import * as Bluebird from 'bluebird';

// remove out if you don't want a Promise polyfill (remove also from webpack.config.js)
// Bluebird.config({ warnings: { wForgottenReturn: false } });

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-animator-css'));
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-html-import-template-loader'));

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => {
//This is the "Offline page" service worker

//Add this below content to your HTML page, or add the js file to your page at the very top to register sercie worker
  //   if (navigator.serviceWorker.controller) {
  //     console.log('[PWA Builder] active service worker found, no need to register');
  //   } else {
  // //Register the ServiceWorker

  //     navigator.serviceWorker.register('pwabuilder-sw.js', {
  //       scope: './'
  //     }).then(function(reg) {
  //       console.log('Service worker has been registered for scope:' + reg.scope);
  //     });
  //   }
    let auth = aurelia.container.get(AuthService);
    let configureHttp = aurelia.container.get(ConfigureHttp);
    configureHttp.configure();
    auth.chkRootForThisUser();
  });
}
