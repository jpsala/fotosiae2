// import {inject} from 'aurelia-framework';
import {Alert} from '../resources/helpers/alert';
import {AuthService} from '../services/auth';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Router} from 'aurelia-router';
import './login.css';
export class Login {
  static inject = [AuthService, Router, EventAggregator];
  email = '';
  password = '';

  constructor(auth, router, ea) {
    this.auth = auth;
    this.router = router;
    this.ea = ea;
  }

  attached() {
    if (this.auth.loggedIn) {
      document.location = '#/';
    }
  }

  async submit() {
    try {
      await this.auth.login(this.email, this.password);
    } catch (e) {
      await Alert.alertify.alert(e);
    }
  }
}
