import {Alert} from '../resources/helpers/alert';
import {HttpClient} from 'aurelia-fetch-client';
import {inject, observable} from 'aurelia-framework';
@inject(HttpClient)
export class Home {
  theStream;
  blob;
  playing;
  @observable matricula;
  matriculaValida;
  constructor(http, authService, ea) {
    this.http = http;
  }
  attached() {
    // let camera = document.getElementById('camera');
    // let frame = document.getElementById('frame');

    // camera.addEventListener('change', function(e) {
    //   let file = e.target.files[0];
    //   // Do something with the image file.
    //   frame.src = URL.createObjectURL(file);
    // });
    this.getStream();
    setTimeout(() => {
      document.getElementById('matricula-input').focus();
    }, 100);
  }
  detached() {
    // Stop all video streams.
    this.videoTracks.forEach(function(track) {track.stop();});
  }

  matriculaChanged(value) {
    this.matriculaValida = this.matricula.length === 4;
  }
  async submit() {
    await this.sube();
    this.blob = false;
    this.matricula = '';
    this.matriculaValida = false;
  }
  async sube() {
    document.getElementById('spinner').style.display = 'block';
    let formData = new FormData();
    formData.append('imagen', this.blob);
    formData.append('matricula', this.matricula);

    const headers =  this.http.defaults.headers;
    this.http.defaults.headers = undefined;
    try {
      let resp = await this.http.fetch('graba-imagen', {
        method: 'POST',
        body: formData
      });
    } catch (error) {
      Alert.alert('Error subiendo la foto');
    }
    document.getElementById('spinner').style.display = 'none';
    this.http.defaults.headers = headers;
  }
  getUserMedia(options, successCallback, failureCallback) {
    let api = navigator.getUserMedia || navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || navigator.msGetUserMedia;
    if (api) {
      return api.bind(navigator)(options, successCallback, failureCallback);
    }
  }

  successCallback(a, b, c) {
    console.log('succ', a, b, c);
  }


  getStream() {
    if (!navigator.getUserMedia && !navigator.webkitGetUserMedia &&
      !navigator.mozGetUserMedia && !navigator.msGetUserMedia) {
      alert('User Media API not supported.');
      return;
    }

    let constraints = {
      video: true
    };

    this.getUserMedia(constraints, (stream)  => {
      let mediaControl = document.querySelector('video');
      if ('srcObject' in mediaControl) {
        mediaControl.srcObject = stream;
        mediaControl.src = (window.URL || window.webkitURL).createObjectURL(stream);
      } else if (navigator.mozGetUserMedia) {
        mediaControl.mozSrcObject = stream;
      }
      this.theStream = stream;
      this.playing = true;
      this.videoTracks = stream.getVideoTracks();
    }, function(err) {
      alert('Error: ' + err);
    });
  }

  takePhoto() {
    if (!('ImageCapture' in window)) {
      alert('ImageCapture is not available');
      return;
    }

    if (!this.theStream) {
      alert('Grab the video stream first!');
      return;
    }

    let theImageCapturer = new ImageCapture(this.theStream.getVideoTracks()[0]);

    theImageCapturer.takePhoto()
      .then(blob => {
        let theImageTag = document.getElementById('imageTag');
        theImageTag.src = URL.createObjectURL(blob);
        this.blob = blob;
      })
      .catch(err => alert('Error: ' + err));
  }
}

// function gotMedia(mediaStream) {
//   const mediaStreamTrack = mediaStream.getVideoTracks()[0];
//   const imageCapture = new ImageCapture(mediaStreamTrack);
//   console.log(imageCapture);
// }
