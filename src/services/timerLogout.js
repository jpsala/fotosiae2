import {inject, observable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {HttpClient} from 'aurelia-fetch-client';
import {AuthService} from '../services/auth';
import {Log} from '../resources/helpers/log';
import {Alert} from '../resources/helpers/alert';

@inject(HttpClient, EventAggregator, AuthService)
export class TimerLogoutService {
  @observable millisecondsForTimeout;
  idleTimeoutHandler;
  pingTimeoutHandler;
  _eventsAdded;
  fnForLogout;
  minutesRemaining;
  constructor(http, ea, authService) {
    this.http = http;
    this.ea = ea;
    this.eventFn = this.eventFn.bind(this);
    this.loginEventFn = this.loginEventFn.bind(this);
    this.logoutEventFn = this.logoutEventFn.bind(this);
    this.idleTimeoutFn = this.idleTimeoutFn.bind(this);
    this.authService = authService;
    this.ea.subscribe('login', this.loginEventFn);
    this.ea.subscribe('logout', this.logoutEventFn);
  }

  init() {
    if (!this.authService.user || !this.millisecondsForTimeout) {
      return;
    }
    this.lastTime = new Date();
    this.addEvents();
    this.eventFn();
  }

  setMillisecondsForTimeout(value) {
    this.millisecondsForTimeout = value;
    this.init();
  }

  loginEventFn(user, data) {
    this.init();
  }

  logoutEventFn() {
    this.logout();
  }
  
  reset() {
    this.removeEvents();
    this.millisecondsForTimeout = undefined;
    this.idleTimeoutHandler && clearTimeout(this.idleTimeoutHandler);
    this.pingTimeoutHandler && clearTimeout(this.pingTimeoutHandler);
    this.remainingInterval && clearInterval(this.remainingInterval);
  }
  
  addEvents(fn) {
    if (this._eventsAdded) return;
    this._eventsAdded = true;
    document.addEventListener('keypress', this.eventFn);
    document.addEventListener('touchstart', this.eventFn);
    document.addEventListener('click', this.eventFn);
    document.addEventListener('scroll', this.eventFn);
    // this.removeEvents();
  }

  removeEvents() {
    this._eventsAdded = false;
    document.removeEventListener('keypress', this.eventFn);
    document.removeEventListener('touchstart', this.eventFn);
    document.removeEventListener('click', this.eventFn);
    document.removeEventListener('scroll', this.eventFn);
  }

  logout() {
    if (!this.authService.loggedIn) return;
    this.reset();
    Alert.alert('Se cerró la sesión por inactividad, ingrese sus credenciales nuevamente');
    this.authService.logout();
  }

  pingServer() {
    console.log('ping server');
    this
      .http
      .fetch('ping', {
      })
      .then((response) => {
        if (response.status === 200) {
          return response;
        }
        throw new Error(response.statusText);
      })
      .catch((err) => {
        console.log(err);
        throw Error('No autorizado');
        // alert(err);
      });
  }

  eventFn(e = null) {
    this.checkForPing();
    this.idleTimeoutHandler && clearTimeout(this.idleTimeoutHandler); 
    this.remainingInterval && clearInterval(this.remainingInterval);
    this.idleTimeoutHandler = setTimeout(this.idleTimeoutFn, this.millisecondsForTimeout);
    this.minutesRemaining = this.millisecondsForTimeout / 60 / 1000;
    this.remainingInterval = setInterval(()=>{
      this.minutesRemaining -= 1;
    }, 60000);
  }

  idleTimeoutFn() {
    this.logout();
  }

  checkForPing() {
    let Diff = (new Date() - this.lastTime);
    if (Diff >= 60000) {// 1 minuto/s
      this.lastTime = new Date();
      this.pingServer();
    }
  }
}
