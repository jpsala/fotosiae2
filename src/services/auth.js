// import {EventAggregator} from 'aurelia-event-aggregator';
import {Aurelia, inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {HttpClient} from 'aurelia-fetch-client';
// import idleTimer from 'idle-timer';
import {  EventAggregator} from 'aurelia-event-aggregator';

const AUTH_TOKEN_NAME = 'iae.fotos.jwt-auth-token';
@inject(HttpClient, Router, Aurelia, EventAggregator)

export class AuthService {
  app = null;
  // _user = {
  //   'nombre': '',
  //   apellido: '',
  //   id: undefined,
  //   saldo: 0,
  //   credito: 100,
  //   socios: []
  // };
  user;
  // idleTimer;
  // idleTimerTimeoutms = 5000;
  loadingAjax;
  minutesRemaining;
  // timeoutms = 1000 * (30 * 60);
  constructor(http, router, aurelia, eventAggregator) {
    this.http = http;
    this.router = router;
    this.storage = window.localStorage;
    this.aurelia = aurelia;
    this.ea = eventAggregator;
    // this.user = this._user;
  }


  routeAuthorized(route) {
    if (!this.loggedIn || (route.admin && !this.user.isAdmin)) {
      return false;
    }
    return true;
  }

  async login(email, password) {
    let that = this;
    // this.idleTimerDestroy();
    await this
      .http
      .fetch('login', {
        method: 'post',
        body: JSON.stringify({
          grant_type: 'password',
          email: email,
          password: password
        })
      })
      .then((response) => {
        if (response.status === 200) {
          return response;
        }
        throw new Error(response.statusText);
      })
      .then((response) => {
        that.chkRootForThisUser(response.user);
      })
      .catch((err) => {
        console.log(err);
        throw Error('No autorizado');
        // alert(err);
      });
  }

  async register(email, password) {
    await this
      .http
      .fetch('/register', {
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then((response) => {
        if (response.status === 200) {
          return response;
        }
        throw response.error;
      })
      .catch((err) => {
        console.log('error', err);
        throw err;
      });
  }

  async activateSocio(socioId, password) {
    try {
      return await this
        .http
        .fetch('/activate', {
          body: JSON.stringify({
            socio_id: socioId,
            password: password
          })
        })
        .then((response) => {
          // console.log('response', response);
          if (response.status === 200) {
            this.chkRootForThisUser(response.user).then(() => {
              document.location = '#/';
            });
            return true;
          }
          // throw new Error(response.statusText);
        });
    } catch (e) {
      throw new Error('Error en activación de socio' + '</br>' + 'Detalle: ' + '</br>' + e);
    }
  }

  async getUserDataForAppInit() {
    return this
      .http
      .fetch('datosSocio').then(s => {
        return s;
      });
  }

  async chkRootForThisUser(user = null) {
    console.log('chkRoot');
    await this.saveOrGetUser(user);
    await this.aurelia.setRoot(PLATFORM.moduleName('app'));
    return;
  }

  async saveOrGetUser(user) {
    if (user) {
      this.user = Object.assign({}, user);
    } else {
      let token = this.getToken();
      // console.log('token', token);
      if (!token) {
        // document.location = '#/login';
        return;
      }
      let response = await this.getUserDataForAppInit();
      this.user = response.user;
    }
    this.ea.publish('login', this.user);
    // this.logoutHandler.init(this.idleTimerTimeoutms, this.alertForTimeoutAndRedirectToLogin.bind(this));
  }

  logout() {
    // this.http
    //   .fetch('/logout');
    if (!this.loggedIn) {
      //this.closeThisWindow();
      console.log('logout: not loggedIn');
    }
    this.cleanAfterLogout();
    this.http
      .fetch('/logout')
      .then((response) => {
        document.location = '#/login';
        return response;
      })
      .catch((err) => {
        console.log('catch en logout', err);
      });
  }

  cleanAfterLogout() {
    console.log('cleanAfterLogout');
    this.storage.removeItem('iae.front.user');
    this.user = undefined;
    // this.user = this._user;
    this.removeToken();
    this.ea.publish('logout');
    // if (this.idleTimer) {
    //   this.idleTimer.destroy();
    // }
    this.chkRootForThisUser();
  }

  get loggedIn() {
    return this.getToken() !== null && this.user;
  }

  getToken() {
    // console.log('AUTH_TOKEN_NAME', AUTH_TOKEN_NAME);
    return this.storage.getItem(AUTH_TOKEN_NAME);
  }

  saveToken(token) {
    this.storage.setItem(AUTH_TOKEN_NAME, token);
  }

  removeToken() {
    this.storage.removeItem(AUTH_TOKEN_NAME);
  }

  /*
  timer
  */
  // resetIdleTimer() {
  //   this.idleTimerDestroy();
  //   this.setIdleTimer();
  // }
  // setIdleTimer() {
  //   console.log('setIdleTimer', this.idleTimerTimeoutms);
  //   this.idleTimerDestroy();
  //   if (this.idleShowMinutesInNavBar) {
  //     clearTimeout(this.idleShowMinutesInNavBar);
  //     this.idleShowMinutesInNavBar = undefined;
  //   }
  //   this.minutesRemaining = (this.idleTimerTimeoutms / 60 / 1000);
  //   this.idleTimer = idleTimer({
  //     callback: this.idleIdleCallbackFn.bind(this),
  //     activeCallback: this.idleActiveCallbackFn.bind(this),
  //     idleTime: this.idleTimerTimeoutms
  //   });
  // }

  // idleIdleCallbackFn() {
  //   this.alertForTimeoutAndRedirectToLogin();
  // }
  // idleActiveCallbackFn() {
  //   console.log("You're Active!");
  // }
  // idleTimerDestroy() {
  //   if (this.idleTimer) this.idleTimer.destroy();
  //   this.idleTimer = undefined;
  // }
  // alertForTimeoutAndRedirectToLogin() {
  //   console.log('Redirect to login', document);
  //   this.logout();
  //   Alert.alert('Se cerró la sesión, ingrese sus credenciales nuevamente');
  // }
  /*
  end timer
  */


}
