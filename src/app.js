import {inject} from 'aurelia-framework';
// import 'semantic-ui-css';
import AuthorizeStep from './resources/helpers/authorize-step';
import {AuthService} from './services/auth';
// import 'semantic-ui-css/semantic.css';
// import './semantic/semantic';
@inject(AuthService)
export class App {
  constructor(auth) {
    this.auth = auth;
  }
  configureRouter(config, router) {
    this.router = router;
    config.title = 'IAE Fotos';
    config.addPipelineStep('authorize', AuthorizeStep);
    let routes = [
      { route: 'home', name: 'home', moduleId: PLATFORM.moduleName('./routes/home'), title: 'Home', nav: true, auth: true },
      { route: 'login', name: 'login', moduleId: PLATFORM.moduleName('./routes/login'), title: 'Login', auth: false },
      { route: '', redirect: 'home' }
    ];
    config.map(routes);

    config.mapUnknownRoutes(() => {
      return PLATFORM.moduleName('./routes/home');
    });

    this.router = router;
  }
  attached() {
  }
}
