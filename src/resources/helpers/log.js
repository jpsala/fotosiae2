export class Log {
  static log(...txt) {
    Log.logAll('black', ...txt);
  }
  static success(...txt) {
    Log.logAll('green', ...txt);
  }
  static info(...txt) {
    Log.logAll('blue', ...txt);
  }
  static error(...txt) {
    Log.logAll('red', ...txt);
  }
  static warn(...txt) {
    // Log.logAll('yellow', ...txt);
    console.warn(...txt);
  }
  static big(txt) {
    console.log(`%c${txt}`, 'background-color: red; color:white; font-weight: bolder; font-size: 16px;');
  }
  static logAll(color, ...txt) {
    txt.forEach((t) => {
      if (typeof(t) === 'object') {
        console.log(t);
      } else {
        console.log('%c' + t, `color:${color}`);
      }
    });
  }
}
