import {inject} from 'aurelia-framework';
import {Config} from './config';
import {Router} from 'aurelia-router';
import {HttpClient} from 'aurelia-fetch-client';
import {AuthService} from '../../services/auth';
import {Alert} from '../../resources/helpers/alert';
import {TimerLogoutService} from '../../services/timerLogout';
@inject(HttpClient, AuthService, Router, TimerLogoutService )
export class ConfigureHttp {
  configured = false;
  loadingAjax = false;
  constructor(http, auth, router, timerLogoutService) {
    this.http = http;
    this.auth = auth;
    this.router = router;
    this.timerLogoutService = timerLogoutService;
  }
  configure() {
    if (this.configured) {
      return;
    }
    this.configured = true;
    let that = this;
    this.http.configure(config => {
      config
        .withBaseUrl(Config.getUrlApi())
        .withDefaults({
          headers: {
            'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          method: 'get'
        })
        .withInterceptor({
          async request(request) {
            that.loadingAjax = true;
            // console.log('that', that.auth.loggedIn);
            // if (that.auth.loggedIn) {
              // console.log('that.auth.getToken()', that.auth.getToken());
              request.headers.append('authorization', `${that.auth.getToken()}`);
            // }
            // console.log('request', request);
            return request;
          },
          response(r) {
            that.loadingAjax = false;
            return r.json()
              .then((response) => {
                // console.log('response', response, response.access_token);
                if (response.status === 401 || response.status === 402) {
                  // that.logoutHandler && that.logoutHandler.reset();
                  that.auth.removeToken();
                  if (response.status === 402) {
                    that.alertForTimeoutAndRedirectToLogin();
                  } else {
                    let ruta = that.router.currentInstruction; //.config.name;
                    // console.log(`${response.status} en interceptor, ruta:`, ruta, 'response:', response);
                    that.router.navigate('#/login');
                  }
                  throw new Error(`Respuesta ${response.status} - ${response.status === 402 ? 'Sesión terminada por inactividad' : 'No autorizado!!!'}`);
                }
                if (response.access_token) {
                  that.auth.saveToken(response.access_token);
                  // console.log('auth.configureAuth().withInterceptor().response().response', response);
                  that.timerLogoutService.setMillisecondsForTimeout(Number(response.minutesForTimeout) * 1000 * 60);
                  // that.resetIdleTimer();
                }
                //  else {
                //   throw new Error('La respuesta del servidor no trajo un token');
                // }
                return response;
              });
          }
        });
    });
  }
  alertForTimeoutAndRedirectToLogin() {
    // console.log('Redirect to login', document);
    this.auth.logout();
    Alert.alert('Se cerró la sesión, ingrese sus credenciales nuevamente');
  }

}
