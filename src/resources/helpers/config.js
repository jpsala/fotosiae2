import {EventAggregator} from 'aurelia-event-aggregator';
import {debounced} from './debounced';
export class Config {
  static inject = [EventAggregator];
  static local = document.location.hostname === 'localhost';
  static dondeEstoy;
  static estoyEnCasa = false;
  windowWidth;
  constructor(ea) {
    this.ea = ea;
    this.ea.subscribe('window-resize', debounced(this.windowResize.bind(this), 100));
    this.dondeEstoy = document.location.hostname;
    if (this.dondeEstoy === 'localhost') {
      this.estoyEnCasa = true;
    }
  }
  windowResize(width) {
    this.windowWidth = width;
  }
  static getUrlBase() {
    let urlBase;
    // console.log(document.location.hostname);
    // return 'http://' + '192.168.2.254' + '/calabres.api/';
    if (document.location.hostname === 'fotos.iae.com.ar' || document.location.hostname === 'app.iae.esc.edu.ar') {
      urlBase = 'https://iaeapi.iae.com.ar/';
      // urlBase = `https://${document.location.hostname}/iae.api/`;
      this.dondeEstoy = 'google';
    } else if (document.location.hostname === 'iae.dyndns.org') {
      urlBase = `http://${document.location.hostname}/iae.api/`;
      this.dondeEstoy = 'cole';
    } else if (document.location.hostname === 'app.iae.com.ar') {
      urlBase = 'https://app.iae.com.ar/iae.api/';
      this.dondeEstoy = 'cole';
    } else if (document.location.hostname === 'localhost') { //casa
      urlBase = 'http://' + document.location.hostname + '/iae.api/';
      this.dondeEstoy = 'casa';
    } else {
      urlBase = 'http://' + document.location.hostname + '/iae.api/';
    }
    return urlBase;
  }

  static getUrlApi() {
    return Config.getUrlBase() + '?r=api/';
  }

  static getUrlImages(width) {
    const subDir = width ? `web-${width}/` : '';
    return Config.getUrlBase() + 'images/' + subDir;
  }

}
