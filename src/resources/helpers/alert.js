import * as miAlertify from 'alertify.js/dist/js/alertify';
import 'alertify.js/dist/css/alertify.css';
import './alert.css';
export class Alert {

  static alertify = miAlertify;

  static async confirm(texto) {
    return new Promise((resolve, reject) => {
      miAlertify
      .okBtn('Ok')
      .cancelBtn('Cancela')
      .confirm(texto,
        () => {
          resolve(true);
        },
        () => {
          resolve(false);
        });
    });
  }
  static error(texto) {
    Alert.alertify.logPosition('top right');
    Alert.alertify.error(texto);
  }
  static log(texto) {
    Alert.alertify.logPosition('top right');
    Alert.alertify.log(texto);
  }
  static success(texto) {
    Alert.alertify.logPosition('top right');
    Alert.alertify.success(texto);
  }
  static async alert(texto, timeout) {
    let timeoutHandler;
    if (timeout) {
      timeoutHandler = setTimeout(()=>{
        console.log('cierro el alerta por timeout');
        $('.alertify .dialog button').click();
      }, timeout);
    }
    return new Promise((resolve, reject) => {
      miAlertify
      .okBtn('Ok')
      .alert(texto,
        () => {
          clearTimeout(timeoutHandler);
          resolve();
        }
      );
    }
  );
  }
}
