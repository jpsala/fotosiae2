import {inject} from 'aurelia-framework';
import {bindable} from 'aurelia-templating';
@inject(Element)
export class Debug {
  @bindable value;
  @bindable height = 'auto';
  valueFormatted = '';

  constructor(element) {
    this.element = element;
  }

  valueChanged(value) {
    console.log('valueChanged', value);
    this.valueFormatted = JSON.stringify(value, null, '\t');
  }

  attached() {
    this.element.querySelector('pre').style.height = this.height;
  }
}
