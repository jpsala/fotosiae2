export class NumberFormatValueConverter {
  toView(val, decimals = 2, currency = false) {
    if (!val) {
      return null;
    }
    let options = {

      minimumFractionDigits: decimals
    };
    if (currency) options.style = 'currency';
    if (currency) options.currency = 'ARS';
    let formatter = new Intl.NumberFormat('es-AR', options);


    // let re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
    // let num = val.toFixed(Math.max(0, ~~n));
    //   // format(100, 2, 3, '.', ',')
    // return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    return formatter.format(parseFloat(val));
  }
}


