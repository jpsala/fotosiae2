export class NumberFilterValueConverter {
  toView(array, propertyName, filter, condition) {
    if (!filter || filter === 'false') {
      return array;
    }
    return array
      .slice(0)
      .filter((a) => {
        return condition ? condition(a[propertyName], filter) : (a[propertyName] === filter);
      });
  }
}
