export class CurrencyFormatValueConverter {
  toView(value = '') {
    if (value === '') return '';
    if (isNaN(value)) value = 0;
    return '$'.concat(Number(value).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
  }
}

