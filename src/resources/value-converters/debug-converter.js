export class DebugValueConverter {
  toView(value) {
    return JSON.stringify(value, null, '\t');
  }
}
