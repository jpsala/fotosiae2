export class UpperValueConverter {
  toView(name) {
    return name && name.toUpperCase();
  }
}