export class LogValueConverter {
  toView(value) {
    let styles = [
      'background: linear-gradient(#FFF, #3E0E02)',
      'border: 1px solid #3E0E02',
      'color: white',
      'display: block',
      'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
      'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
      'line-height: 20px',
      'text-align: center',
      'font-weight: bold',
      'padding: 2px 2px'
    ].join(';');
    // let  a = InfiniteJSON.stringify( value );
    // console.info('debug %O', value);
    console.log('%c Debug %c %o', styles, 'color: green', value );
    // console.log(a);
    // console.log(CircularJSON.parse(CircularJSON.stringify(value)));
    // let json = CircularJson.parse(value);
    // let result = sc(value);
    // console.log(JSON.stringify(result, null, 2));
    // return JSON.stringify(a, '\t', 2);
  }
}
